import { Component, OnInit, OnChanges, Input, Output, EventEmitter } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-form-modal',
  templateUrl: './form-modal.component.html',
  styleUrls: ['./form-modal.component.css']
})
export class FormModalComponent implements OnInit {

  @Input() id: string;
  componentActive: string;
 // myForm: FormGroup;

  constructor(
    public activeModal: NgbActiveModal, 
    ) { 
  }

  ngOnInit() {   
    this.componentActive=this.id;     
  }

  
  // private submitForm() {
  //   this.activeModal.close(this.myForm.value);
  // }

  closeModal() {
    this.activeModal.close('Modal Closed');
  }

  // isActive(component){
 
  //   if(component == this.id){
  //     this.componentActive=component;      
  //     return true;
  //   }else{
  //     return false;
  //   }
  // }

  displayComponent(component){
    this.componentActive=component;    
  }

}
