import { RouterModule, Routes } from "@angular/router";

// Components
import { NopagefoundComponent } from './shared/components/nopagefound/nopagefound.component';
import { FormComponent } from './form/form.component';


const appRoutes: Routes=[
    { path:'form',component: FormComponent },
    { path:'', redirectTo:'/form', pathMatch:'full' },
    { path:'**', component: NopagefoundComponent, data: { i18n:'not-found', json:'common'}, }
];

export const APP_ROUTES=RouterModule.forRoot(appRoutes,{useHash:true});