import { NgModule }       from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

// Components
import { UserComponent }        from './user.component';
import { LoginComponent }        from './login/login.component';
import { SignUpComponent }        from './signup/signup.component';

// Modules
import { SharedModule } from '../shared/components/shared.module';

// Routing
import { USER_ROUTES } from './user.routing';


@NgModule({
  imports: [ USER_ROUTES, SharedModule, FormsModule, ReactiveFormsModule ],
  exports: [ UserComponent, LoginComponent,SignUpComponent ],
  declarations: [ UserComponent, LoginComponent,SignUpComponent ]
})
export class UserModule { }