import { RouterModule, Routes } from "@angular/router";

import { UserComponent }        from './user.component'; // Parent Component
import { LoginComponent }        from './login/login.component';
import { SignUpComponent }        from './signup/signup.component';


const pagesRoutes: Routes=[
    {
      path: 'user',
      component: UserComponent,
      data: { i18n:'title', json:'register'},
      children: [
        { path: '', redirectTo: 'login', pathMatch: 'full'},
        { path: 'login', component: LoginComponent, data: { i18n:'title', json:'login'} },
        { path: 'signup', component: SignUpComponent }
      ]
    }
];

export const USER_ROUTES=RouterModule.forChild(pagesRoutes);