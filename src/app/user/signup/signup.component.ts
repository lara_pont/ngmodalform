import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';

// import { UserComponent } from '../user.component';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styles: []
})

export class SignUpComponent implements OnInit {
  model: any = [];
  success = false;
  errors;

  formSignup: FormGroup;
  passPattern:string;
  emailPattern:string;

  @Output() componentChange = new EventEmitter();
  @Input() idModal: string;

  constructor(
    private router: Router,
    // private user: UserComponent
  ) { 
    this.passPattern="^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])[^ ]{8,}$";
    this.emailPattern="^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$";
  }

  ngOnInit(){
    this.formSignup = new FormGroup({
      name: new FormControl( null , [ Validators.required] ),
      email: new FormControl( null , [ Validators.required, Validators.pattern(this.emailPattern)] ),
      password1: new FormControl( null , [ Validators.required, Validators.pattern(this.passPattern)] ),
      password2: new FormControl( null , [ Validators.required, Validators.pattern(this.passPattern)] ),
      terms: new FormControl(false),
      mailing: new FormControl(false)
    });
  }

  signup() {

    if ( this.formSignup.invalid ) {
      return;
    }

    if ( !this.formSignup.value.terms ) { 
      this.errors = 'terms-ko'; // Mandatory Terms
      return;
    }
    
    // this._auth.signup(
    //   this.formSignup.controls.name.value,
    //   this.formSignup.controls.email.value,
    //   this.formSignup.controls.password1.value
    // ).subscribe(
    //   result => {
    //     this.success = true;
    //     // Only in production, send the 'registration completed' FB event
    //     if(environment.production) {
    //       fbq('track', 'CompleteRegistration');
    //       ga('send', 'event', 'Users', 'Signup');
    //       adwords(signup);
    //     }
    //   },
    //   error => this.errors = error
    // )
  }

  ingresarAhora(event){    
    if(this.idModal != undefined){
      let componente='app-login';
      this.componentChange.emit(componente);  
    }else{
      this.router.navigate(['/user/login']);
    }    
  }

}
