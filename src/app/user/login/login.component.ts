import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styles: [],
})
export class LoginComponent implements OnInit{
  model: any = [];
  loading = false;

  @Output() componentChange = new EventEmitter();
  @Input() idModal: string;

  constructor(
    private router: Router
  ) {
    
  }

  ngOnInit(){ }

  login() {
    this.loading = true;
    localStorage.clear();
  }

  crearCuenta(event){        
    if(this.idModal != undefined){
      let componente='app-signup';
      this.componentChange.emit(componente);  
    }else{
      this.router.navigate(['/user/signup']);
    }    
  }

}
