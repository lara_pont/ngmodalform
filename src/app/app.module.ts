import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { APP_ROUTES } from './app.routing';

import { AppComponent } from './app.component';
import { FormModalComponent } from './form-modal/form-modal.component';


import { UserModule } from './user/user.module';
import { FormComponent } from './form/form.component';


@NgModule({
  declarations: [
    AppComponent,
    FormModalComponent,
    FormComponent,
  ],
  imports: [
    BrowserModule,
    APP_ROUTES,
    NgbModule.forRoot(),    
    FormsModule,
    ReactiveFormsModule,
    UserModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
  entryComponents: [
    FormModalComponent
  ]
})
export class AppModule { }
