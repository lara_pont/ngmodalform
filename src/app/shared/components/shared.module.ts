import { NgModule }     from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

// import { NgbModule }    from '@ng-bootstrap/ng-bootstrap';
// import { FormsModule, ReactiveFormsModule } from '@angular/forms';

// Components
import { NopagefoundComponent } from './nopagefound/nopagefound.component';


@NgModule({
  declarations: [
    NopagefoundComponent,    
  ],
  imports: [
    CommonModule,
    RouterModule,
    // NgbModule,
    // FormsModule,
    // ReactiveFormsModule,
  ],
  exports:[
    CommonModule,
    NopagefoundComponent,
    // NgbModule,
    // FormsModule,
    // ReactiveFormsModule,
    
  ],
  // entryComponents: [
  //   ModalContent
  // ]
})
export class SharedModule { }